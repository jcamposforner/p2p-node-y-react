'use strict'

var express = require('express');
var HistorialController = require('../controllers/historial');

var md_auth = require('../middlewares/authenticated');
var api = express.Router();

// Subida imagen

var multipart = require('connect-multiparty');



api.get('/my-historial/:page?',md_auth.ensureAuth, HistorialController.getMyHistorial);
api.post('/add-historial',md_auth.ensureAuth, HistorialController.saveHistorial);


module.exports = api;