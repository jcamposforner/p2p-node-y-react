'use strict'

var express = require('express');
var CommentController = require('../controllers/comments');

var md_auth = require('../middlewares/authenticated');
var api = express.Router();


api.get('/get-comments/:id/:page?', CommentController.getComments);

api.post('/add-comment/:id',md_auth.ensureAuth, CommentController.saveComment);


module.exports = api;