'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var deepPopulate = require('mongoose-deep-populate')(mongoose);

var VotesSchema = Schema({
    user:{ type: Schema.ObjectId, ref: 'User' },
    media: { type: Schema.ObjectId, ref: 'Media' },
    created_at: String,
});

VotesSchema.plugin(deepPopulate)
module.exports = mongoose.model('Votes', VotesSchema);