'use strict'

var mongoosePaginate = require('mongoose-pagination');
var moment = require('moment');


// Primera letra mayuscula para saber que es un modelo
var Comment = require('../models/comments');
var jwt = require('../services/jwt');


function saveComment(req,res){
    //Campos de POST
    var params = req.body;
    var mediaId = req.params.id;
    // Creamos nuevo usuario
    var comment = new Comment();

    if(params.body){

        comment.media = mediaId;
        comment.body = params.body;
        comment.user = req.user.sub;
        comment.created_at = moment().unix();
        
        comment.save((err,commentStored) => {
            if(err) return res.status(500).send({message:'Ha ocurrido un error al guardar el comentario'});

            if(commentStored){
                res.status(200).send({comment: commentStored});
            }else{
                res.status(404).send({message:'No se ha registrado el comentario'});
            }
        });
    }else{
        res.status(200).send({
           message: 'Rellena todos los datos',
        });
    }

}

function getComments(req,res){

    var mediaId = req.params.id;
    var page = 1;
    if(req.params.page){
        page = req.params.page;
    }

    var itemsPerPage = 10;

    Comment.find({media:mediaId}).select('-_id -__v -media').sort('-created_at').populate('user','-password -role -__v').paginate(page, itemsPerPage, (err,comments, total) => {
        if(err) return res.status(500).send({message: 'Error en la petición'});

        if(!comments) return res.status(404).send({message: 'No hay comentarios disponibles'});

        return res.status(200).send({
            comments,
            currentPage:page,
            total,
            pages: Math.ceil(total/itemsPerPage),
        });
    });
}

module.exports = {
    saveComment,
    getComments
};