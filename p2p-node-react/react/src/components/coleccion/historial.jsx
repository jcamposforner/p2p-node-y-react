import React,{Component} from 'react';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import axios from 'axios';
import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import FavoriteIcon from '@material-ui/icons/Favorite';
import AudioPlayer from "react-h5-audio-player";
import VideoPlayer from './VideoPlayer';
import { Link } from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify';

const urlApi = 'http://localhost:3800/api/';

const styles = theme => ({
    main: {
      width: 'auto',
      marginTop:theme.spacing.unit*2,
      display: 'block', // Fix IE 11 issue.
      marginLeft: theme.spacing.unit * 3,
      marginRight: theme.spacing.unit * 3,
      [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
        marginLeft: 'auto',
        marginRight: 'auto',
      },
    },
    content: {
        marginTop:theme.spacing.unit * 2,
        marginLeft: theme.spacing.unit * 3,
    },
    root: {
        flexGrow: 1,
      },
      paper: {
        height: 140,
        width: 100,
      },
      control: {
        padding: theme.spacing.unit * 2,
      },
  });

class Historial extends Component {

    constructor(props){
        super(props)

        this.state = {
            medias: [],
            token: JSON.parse(localStorage.getItem('token')),
            identity: JSON.parse(localStorage.getItem('identity'))
        }
    }

    likeMedia(mediaId){
        
        const data = {
            media: mediaId,
        }
        axios.post(`${urlApi}add-like`,data,{
            headers: {
                'Content-Type':'application/json',
                'Authorization': JSON.parse(localStorage.getItem('token'))
            }
        }).then((res) =>{
            
            if(res.data.voto){
                this.notifyLike('Has votado con exito!');
            }else{
                this.notifyLike(res.data.message,'error');
            }
        },(err)=>{
            console.log(err)
        })
    }

    notifyLike = (message,tipo) => {
        if(tipo == 'error'){
            toast.error(message)
        }else{
            toast.info(message)
        }
    };

    componentWillMount(){
        axios.get(`${urlApi}my-historial`, {headers:{
            'Content-Type':'application/json',
            'Authorization': this.state.token
        }}).then((res)=>{
            this.setState({
                medias: res.data.videos,
                currentPage: res.data.currentPage,
                pages: res.data.pages,
                total: res.data.total
            })
        })
    }
    // solo 4 canciones en resumen
    
    render() {
        const { classes,name } = this.props;
        const { medias,total,pages,currentPage } = this.state


        const rows = [...Array( Math.ceil(medias.length / 3) )];

        const productRows = rows.map( (row, idx) => medias.slice(idx * 3, idx * 3 + 3) );
        const content = productRows.map((row, idx) => (
            <div className="columns" key={idx}>
                { row.map( media =>
                    <div key={media._id} className="column is-4">
                            <div className="card profile-card">
                                <div className="card-image">
                                        {
                                        media.media.type == 'Video' ? 
                                        
                                        <VideoPlayer mediaId={media.media._id} thumbmail={urlApi+"get-thumbmail/"+media.media.thumbmail} media={urlApi+"get-media/"+media.media.media}></VideoPlayer>
                                        
                                        :
                                        <div>
                                        <AudioPlayer
                                            controls
                                            src={urlApi+"get-media/"+media.media.media}
                                            onPlay={e => console.log("onPlay")}
                                        />
                                        </div>
                                        }
                                        

                                </div>
                                <div className="card-content">
                                    <Link className="link-profile" to={`/media/${media.media.slug}`}>
                                        <div className="content">
                                            Titulo: <b> {media.media.title}</b>
                                            <br/>
                                            Autor: <b> {media.media.user.nick} </b>
                                        </div>
                                    </Link>
                                </div>
                                {media.media.type == 'Video' ? 
                                    <div>
                                        <BottomNavigation>
                                            <BottomNavigationAction onClick={()=>this.likeMedia(media.media._id)} label="Favorites" icon={<FavoriteIcon />} />
                                        </BottomNavigation>
                                    </div> 
                                    :
                                    <div className="like-profile">
                                        <BottomNavigation>
                                            <BottomNavigationAction onClick={()=>this.likeMedia(media.media._id)} label="Favorites" icon={<FavoriteIcon />} />
                                        </BottomNavigation>
                                    </div>
                                }
                                
                            </div>
                    </div> )}
            </div> )
        );

        return (
            <div>
                <ToastContainer />
                <Typography className={classes.content} paragraph color="textPrimary" component="h2" variant="h4" align="center">
                    Videos que has visto ({total})
                </Typography>
                {content.length > 0 ? (
                    <div>{content}</div>
                ): 
                <Typography className={classes.content} paragraph color="textSecondary" component="h3" variant="h6" align="center">
                    No te gusta ningún video
                </Typography>
                    
                }
            </div>
            

        );



    }



}

export default withStyles(styles)(Historial);