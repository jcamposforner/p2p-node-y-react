import React, {Component} from 'react';
import { BrowserRouter as Router, Route, Link, Redirect, withRouter,Switch } from 'react-router-dom';
import NavBar from './header/navbar';
import Footer from './footer';
import Login from './login/index';
import Registro from './register/index';
import HomeNotLogged from './home/notlogged';
import HomeLogged from './home/logged';
import Coleccion from './coleccion/'
import Cancion from './cancion';
import UploadMedia from './upload';
import MyAccount from './account';
import Perfil from './perfil';
import Search from './search';
import Chat from './chat'
import Profile from './profile';
import Streaming from './streaming';
import StreamingStart from './streamingstart';


// PARA QUE NO TENGA HEADER EL ADMIN
const DefaultRouter = () => (
    <Router>
        <Switch>
            <Route component={StaticSite} />
        </Switch>
    </Router>
)


const StaticSite = () => (
            <Router>
                <div>
                    <NavBar></NavBar>
                    {(localStorage.getItem('auth') ? (
                        <Chat></Chat>
                    ):null)}
                    <Switch>
                        <Route exact path="/" component={HomeComponent} />
                        <Route exact path="/stream/start" component={StreamingStartComponent} />
                        <Route exact path="/stream/:id?" component={StreamingComponent} />
                        <Route exact path="/coleccion/:tipo?/:page?" component={ColeccionComponent} />
                        <Route exact path="/mi-perfil/:page?" component={PerfilComponent} />
                        <Route exact path="/perfil/:id/:page?" component={ProfileComponent} />
                        <Route exact path="/media/:slug" component={CancionComponent} />
                        <Route exact path="/login" component={LoginComponent} />
                        <Route exact path="/my-account" component={MyAccountComponent} />
                        <Route exact path="/upload" component={UploadComponent} />
                        <Route exact path="/registro" component={RegistroComponent} />
                        <Route exact path="/search" component={SearchComponent} />
                    </Switch>
                    <Footer></Footer>
                </div>

            </Router>
)

const HomeComponent = (location) => (
    localStorage.getItem('auth') ? (
        <HomeLogged></HomeLogged>
    ):<HomeNotLogged></HomeNotLogged>
)

const SearchComponent = (location) => (
    location.location.state ? (
        <Search location={location.location}></Search>
    ):<HomeComponent></HomeComponent>
)

const MyAccountComponent = () => (
    localStorage.getItem('auth') ? (
        <MyAccount></MyAccount>
    ):<Login></Login>
)

const UploadComponent = () => (
    localStorage.getItem('auth') ? (
        <UploadMedia></UploadMedia>
    ):<HomeNotLogged></HomeNotLogged>
    
)

const PerfilComponent = (location) => (
    localStorage.getItem('auth') ? (
        <Perfil location={location}></Perfil>
    ):<Login></Login>
)

const ProfileComponent = (location, match) => (
    localStorage.getItem('auth') ? (
        <Profile match={match.params} location={location}></Profile>
    ):<Login></Login>
)


const CancionComponent = (match) => (
    localStorage.getItem('auth') ? (
        <Cancion slug={match.match.params.slug}></Cancion>
    ):<Redirect to="/login"></Redirect>
)

const StreamingStartComponent = (match) => (
    localStorage.getItem('auth') ? (
        <StreamingStart match={match}></StreamingStart>
    ):<Redirect to="/login"></Redirect>
)

const StreamingComponent = (match) => (
    localStorage.getItem('auth') ? (
        <Streaming match={match}></Streaming>
    ):<Redirect to="/login"></Redirect>
)

const ColeccionComponent = (match) => (
    localStorage.getItem('auth') ? (
        <Coleccion match={match}></Coleccion>
    ):<Redirect to="/login"></Redirect>
)

const LoginComponent = () => (
    localStorage.getItem('auth') ? (
    
    <Redirect to="/"></Redirect>
    ):
    <Login></Login>
)

const RegistroComponent = () => (
    localStorage.getItem('auth') ? (
    
        <Redirect to="/"></Redirect>
        ):
        <Registro></Registro>
)

export default DefaultRouter;