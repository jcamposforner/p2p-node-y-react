'use strict'

var express = require('express');
var FollowController = require('../controllers/siguiendo');

var md_auth = require('../middlewares/authenticated');
var api = express.Router();


api.get('/get-all-follows',md_auth.ensureAuth, FollowController.getAllFollows);
api.get('/get-following/:id?/:page?',md_auth.ensureAuth, FollowController.getPeopleIFollow);
api.get('/get-follows/:id?/:page?',md_auth.ensureAuth, FollowController.getPeopleWhoFollowsMe);
api.get('/get-medias-following/:page?',md_auth.ensureAuth, FollowController.getMediasFollowing);


api.post('/add-follow',md_auth.ensureAuth, FollowController.saveFollow);
api.post('/unfollow',md_auth.ensureAuth, FollowController.unFollow);


module.exports = api;