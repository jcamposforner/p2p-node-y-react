'use strict'

var mongoosePaginate = require('mongoose-pagination');
var moment = require('moment');
var async = require('async');




// Primera letra mayuscula para saber que es un modelo
var Follow = require('../models/siguiendo');
var User = require('../models/user');
var Media = require('../models/media');


function saveFollow(req,res){
    //Campos de POST
    var params = req.body;



    // Creamos nuevo usuario
    var follow = new Follow();
    if(params.id){
        follow.user = req.user.sub;
        follow.siguiendo = params.id;
        follow.created_at = moment().unix();

        // Comprobar usuarios duplicados

        Follow.find({ $and: [
            {user: req.user.sub},
            {siguiendo:follow.siguiendo}
        ]}).exec((err,follows) => {
            if(err) return res.status(500).send({message:'Error en la petición de seguimiento'});

            if(follows && follows.length >= 1){
                return res.status(200).send({message: 'Ya estas siguiendo a este usuario'});
            }else{

                follow.save((err,followStored) => {
                    if(err) return res.status(500).send({message:'Ha ocurrido un error al guardar el usuario'});

                    if(followStored){
                        res.status(200).send({follow: followStored});
                    }else{
                        res.status(404).send({message:'No se ha registrado el usuario'});
                    }
                });
            }
        });
    }
    
}

function unFollow(req,res){
//Campos de POST
var params = req.body;



    if(params.id){
        

        // Comprobar usuarios duplicados

        Follow.findOneAndRemove({ $and: [
            {user: req.user.sub},
            {siguiendo:params.id}
        ]}).exec((err,followRemoved) => {
            if(err) return res.status(500).send({message:'Error en la petición de seguimiento'});

            if(!followRemoved) return res.status(404).send({message:'Error al borrar el seguimiento'});

            return res.status(200).send({
                followRemoved
            })
        });
}
}


function getAllFollows(req,res){

    var userId = req.user.sub;


    Follow.find({user:userId}).select('-__v -_id').populate('siguiendo', '-__v -password -role').exec((err, following) =>{
        if(err) return res.status(500).send({message: 'Error en la petición'});

        if(!following) return res.status(404).send({message: 'No sigues a nadie'});

        Follow.find({siguiendo:userId}).select('-__v -_id').populate('siguiendo', '-__v -password -role').exec((err, follows) =>{
            if(err) return res.status(500).send({message: 'Error en la petición'});
    
            if(!follows) return res.status(404).send({message: 'No te sigue nadie'});
    
            return res.status(200).send({
                follows,
                following
            });
        });
    });

    

}


function getMediasFollowing(req,res){

    var userId = req.user.sub;
    var array = [];
    var j=0;
    var page = 1;
    if(req.params.page){
        page = req.params.page
    }
    var itemsPerPage = 12;
    Follow.find({user:userId}).exec((err,peoples)=>{
        peoples.forEach((people,idx) => {
            var query = Media.find({user:people.siguiendo}).sort('-created_at').populate('user','-password -role -_id');
            query.then((doc) => {
                array.push(doc);
            }).finally((doc)=>{
                for(let i = 0; i < array.length; i++){
                    var merge = array[0].concat(array[i])
                }
                j += 1;
                if(j == peoples.length){
                    
                    return res.status(200).send({
                        medias:paginateArray(merge,itemsPerPage,page),
                        total: merge.length,
                        pages: Math.ceil(merge.length/itemsPerPage),
                        actualPage: page,
                    })
                }
            });
        });

    }) ;
}
function paginateArray(array, itemsPerPage, page){
    --page;
    return array.slice(page * itemsPerPage, (page + 1) * itemsPerPage);
}

function getPeopleIFollow(req,res){

    var userId = req.user.sub;
    var page = 1;


    if(req.params.id){
        userId = req.params.id;
    }

    if(req.params.page){
        page = req.params.page;
    }

    var itemsPerPage = 10;

    Follow.find({user:userId}).select('-__v -_id').sort('-created_at').populate('siguiendo','-__v -password -role').paginate(page, itemsPerPage, (err,following, total) => {
        if(err) return res.status(500).send({message: 'Error en la petición'});

        if(!following) return res.status(404).send({message: 'No sigues a nadie'});

        return res.status(200).send({
            following,
            total,
            pages: Math.ceil(total/itemsPerPage),
            currentPage:page,
        });
    });
}

function getPeopleWhoFollowsMe(req,res){
    var userId = req.user.sub;
    var page = 1;


    if(req.params.id){
        userId = req.params.id;
    }

    if(req.params.page){
        page = req.params.page;
    }

    var itemsPerPage = 10;

    Follow.find({siguiendo:userId}).select('-__v -_id').sort('-created_at').populate('user','-__v -password -role').paginate(page, itemsPerPage, (err,following, total) => {
        if(err) return res.status(500).send({message: 'Error en la petición'});

        if(!following) return res.status(404).send({message: 'No te sigue nadie'});

        return res.status(200).send({
            following,
            total,
            pages: Math.ceil(total/itemsPerPage),
            currentPage:page,
        });
    });
}

module.exports = {
    saveFollow,
    getPeopleWhoFollowsMe,
    getPeopleIFollow,
    unFollow,
    getAllFollows,
    getMediasFollowing
};