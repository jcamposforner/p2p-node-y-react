'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var VotesSchema = Schema({
    user:{ type: Schema.ObjectId, ref: 'User' },
    comment: { type: Schema.ObjectId, ref: 'Comments' },
    rated: Number,
});


module.exports = mongoose.model('Votes', VotesSchema);