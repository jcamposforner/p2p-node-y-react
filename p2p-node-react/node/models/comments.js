'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var CommentsSchema = Schema({
    user:{ type: Schema.ObjectId, ref: 'User' },
    media:{ type: Schema.ObjectId, ref: 'Media' },
    body: String,
    created_at: String,
});


module.exports = mongoose.model('Comments', CommentsSchema);