import React,{Component} from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { urlApi } from '../../env'
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import axios from 'axios';
import AudioPlayer from "react-h5-audio-player";
import VideoPlayer from './VideoPlayer';
import { Link } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import Moment from "react-moment";
import 'moment-timezone';
import FormComments from './formComment';

const styles = theme => ({
    main: {
      width: 'auto',
      display: 'block', // Fix IE 11 issue.
      marginLeft: theme.spacing.unit * 3,
      marginTop: theme.spacing.unit *3,
      marginRight: theme.spacing.unit * 3,
      [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
        marginLeft: theme.spacing.unit * 3,
        marginRight: theme.spacing.unit * 3,
      },
    },
    paper: {
      marginTop: theme.spacing.unit * 8,
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
    },
    avatar: {
      margin: theme.spacing.unit,
      backgroundColor: theme.palette.secondary.main,
    },
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing.unit,
    },
    submit: {
      marginTop: theme.spacing.unit * 3,
    },
    button: {
        margin: theme.spacing.unit,
        width: '100%',
    },
    avatar: {
        margin: 10,
      },
      bigAvatar: {
        margin: 10,
        width: 100,
        height: 100,
      },
      smallAvatar: {
        margin: 10,
        width: 60,
        height: 60,
      },
  });


class Cancion extends Component {

    constructor(props){
        super(props)
        this.state = {
            type:'',
            identity:JSON.parse(localStorage.getItem('identity')),
            token:JSON.parse(localStorage.getItem('token')),
            media:[],
            user:'',
            actualPage:1,
            comments:[],
            loadingComments: false,
        };
    }


    async componentWillMount(){
        window.scrollTo(0, 0);
        var slug = this.props.match.params.slug;
        await axios.get(`${urlApi}media/${slug}`, {
            headers : {
               'Content-Type':'application/json',
               'Authorization': this.state.token
               }
           }).then(
               (res) => {
                   this.setState({
                        media: res.data.media[0],
                        user: res.data.media[0].user
                   })
               },(err) =>{
                   console.log(err)
               }
       )
    }

    loadComments(){
        this.setState({
            loadingComments:true,
        })
        if(this.state.comments.length > 0){
            this.setState({
                loadingComments:false,
                comments: [],
                actualPage: 1,
            })
            return false;
        }
        axios.get(`${urlApi}get-comments/${this.state.media._id}`).then(
            (res) =>{
                this.setState({
                    loadingComments:false,
                    comments: res.data.comments,
                    currentPage: res.data.currentPage,
                    total: res.data.total,
                    pages: res.data.pages,
                })
            },(err)=>{
                console.log(err)
            }
        )
        
    }
    loadMoreComments(){
        this.setState({
            loadingComments:true,
        })
        var nextPage = this.state.actualPage+1;
        axios.get(`${urlApi}get-comments/${this.state.media._id}/${nextPage}`).then(
            (res) =>{
                console.log(res.data)
                    // AÑADIR AL ARRAY DE OBJECTOS DE COMMENTS LOS DE AQUI !!!!! IMPORTANTE
                
                this.setState({
                    loadingComments:false,
                    comments: res.data.comments,
                    actualPage:this.state.actualPage+1,
                    currentPage: res.data.currentPage,
                    total: res.data.total,
                    pages: res.data.pages,
                })
            },(err)=>{
                console.log(err)
            }
        )
    }
    loadLessComments(){
        this.setState({
            loadingComments:true,
        })
        var nextPage = this.state.actualPage-1;
        axios.get(`${urlApi}get-comments/${this.state.media._id}/${nextPage}`).then(
            (res) =>{
                this.setState({
                    loadingComments:false,
                    comments: res.data.comments,
                    actualPage:this.state.actualPage-1,
                    currentPage: res.data.currentPage,
                    total: res.data.total,
                    pages: res.data.pages,
                })
            },(err)=>{
                console.log(err)
            }
        )
    }


        
    render (){
        const { classes } = this.props;
        const { user,media,loadingComments,comments,currentPage,pages } = this.state;
        console.log(user)
        return (
            <div style={{marginTop:'2rem'}}>
                
                <div className="container">
                    <div className="columns">
                        <div className="column is-12">
                            <Typography component="h2" variant="h2" align="center" color="textPrimary">
                                {media.title}
                            </Typography>
                        </div>
                    </div>

                        {/* Content */}
                        { media.media && (
                            <div className="columns">
                                <div className="column is-12">
                                    <VideoPlayer thumbmail={urlApi+"get-thumbmail/"+media.thumbmail} media={urlApi+"get-media/"+media.media}></VideoPlayer>
                                </div>
                            </div>
                        )}


                        <div className="columns">
                            <div style={{borderRight:'1px solid #d4d4d4'}} className="column is-6">

                                <div className="columns">
                                    <div className="column is-12">
                                        
                                    <article className="media">
                                        <figure className="media-left">
                                                <Avatar alt={user._id} src={urlApi+"get-image-user/"+user.image} className={classes.bigAvatar} />
                                            
                                        </figure>
                                        <div className="media-content">
                                            <div>
                                                <Link to={"/perfil/"+user._id}>
                                                    <Typography component="h2" variant="h4" align="right" color="textPrimary">
                                                        {user.nick}
                                                    </Typography>
                                                    <Typography component="h3" variant="h7" align="right" color="textSecondary">
                                                        {user.name} {user.surname}
                                                    </Typography>
                                                </Link>
                                            </div>
                                        </div>
                                    </article>

                                    </div>
                                </div>


                            </div>
                            <div className="column is-6">
                                <Typography component="h2" variant="h4" align="left" color="textPrimary">
                                    Descripción
                                </Typography>
                                <Typography component="h3" variant="h7" align="left" color="textSecondary">
                                    <span dangerouslySetInnerHTML={{__html:media.description}}></span>
                                </Typography>
                            </div>
                        </div>

                        <div className="columns">
                            <div className="column is-12">
                                
                                
                                <Button style={{height:'50px'}} onClick={this.loadComments.bind(this)} variant="contained" color="secondary" className={classes.button}>
                                    { comments.length > 0 ?  <span>Ocultar Comentarios</span> : <span>Ver Comentarios</span> }
                                    { loadingComments && (
                                        <div style={{position:'absolute'}}>
                                            <CircularProgress></CircularProgress>
                                        </div>
                                    )}
                                </Button>
                                <hr></hr>
                            </div>
                        </div>
                            { 
                                comments.map((comment) =>

                                    <div style={{borderBottom:'1px solid #d4d4d4'}} className="columns" key={comment._id}>
                                        
                                        <div className="column is-12">
                                            <article className="media">
                                                <figure className="media-left">
                                                    
                                                        <Avatar alt={comment.user._id} src={urlApi+"get-image-user/"+comment.user.image} className={classes.smallAvatar} />
                                                    
                                                </figure>
                                                <div className="media-content">
                                                    <div className="content">
                                                    <p>
                                                    {/* Falta link de perfil del usuario */}
                                                        <strong>{comment.user.name} {comment.user.surname}</strong> <small>@{comment.user.nick}</small> 
                                                        <br />
                                                        {comment.body}
                                                    </p>
                                                    </div>
                                                    <nav className="level is-mobile">
                                                    <div className="level-left">
                                                        <p style={{cursor:'pointer'}} className="level-item">
                                                            <span className="icon is-small"><i className="fas fa-heart"></i></span>
                                                        </p>
                                                    </div>
                                                    </nav>
                                                </div>
                                                <div className="media-right">
                                                    <small>
                                                        <Moment fromNow unix>{comment.created_at}</Moment>
                                                    </small>
                                                </div>
                                            </article>
                                        </div>

                                    </div>
                                
                                )
                            }
                            { comments.length > 0 && (
                                <div className="columns">
                                    <div className="column is-6">

                                        { currentPage == 1 ? null :
                                            <Button 
                                                style={{height:'50px'}} 
                                                onClick={this.loadLessComments.bind(this)} 
                                                variant="contained" 
                                                color="secondary" 
                                                className={classes.button}
                                            >
                                                Anterior
                                            </Button>
                                        }

                                    </div>
                                    <div className="column is-6">



                                        {
                                            currentPage == pages ? null :
                                        
                                            <Button 
                                                style={{height:'50px'}} 
                                                onClick={this.loadMoreComments.bind(this)} 
                                                variant="contained" 
                                                color="secondary" 
                                                className={classes.button}
                                            >
                                                Siguiente
                                            </Button>
                                        }
                                    </div>
                                </div>
                                
                                
                            )}

                            <FormComments mediaId={media._id} identity={this.state.identity} token={this.state.token}></FormComments>
                            
                            
                    
                    <hr />
                </div>
            </div>
        );
    };
}
Cancion.propTypes = {
    classes: PropTypes.object.isRequired,
  };


export default 
    withStyles(styles)(
    (withRouter(Cancion))
);