'use strict'

var mongoosePaginate = require('mongoose-pagination');
var bcrypt = require('bcrypt-nodejs');
var fs = require('fs');
var path = require('path');
var slugify = require('slugify')




// Primera letra mayuscula para saber que es un modelo
var User = require('../models/user');
var Follow = require('../models/siguiendo');
var jwt = require('../services/jwt');



function home(req,res){
    res.status(200).send({
        message: 'Home express'
    });
}


function saveUser(req,res){
    //Campos de POST
    var params = req.body;

    // Creamos nuevo usuario
    var user = new User();

    if(params.name && params.surname && params.email && params.password && params.nick){

        user.name = params.name;
        user.surname = params.surname;
        user.email = params.email;
        user.role = 'ROLE_USER';
        user.nick = params.nick;
        user.image = 'default-avatar.png';
        user.slug = slugify(params.nick, { replacement: '-', remove: null, lower: true});

        // Comprobar usuarios duplicados

        User.find({ $or: [
            {email: user.email.toLowerCase()},
            {nick:user.nick.toLowerCase()}
        ]}).exec((err,users) => {
            if(err) return res.status(500).send({message:'Error en la petición de usuarios'});

            if(users && users.length >= 1){
                return res.status(200).send({message: 'El usuario ya existe'});
            }else{
                bcrypt.hash(params.password,null,null, (err,hash) => {
                    user.password = hash;

                    user.save((err,userStored) => {
                        if(err) return res.status(500).send({message:'Ha ocurrido un error al guardar el usuario'});

                        if(userStored){
                            res.status(200).send({user: userStored});
                        }else{
                            res.status(404).send({message:'No se ha registrado el usuario'});
                        }
                    });
                });
            }
        });





    }else{
        res.status(200).send({
           message: 'Rellena todos los datos',
        });
    }

}


function loginUser(req,res){

    var params = req.body;

    var email = params.email;


    User.findOne({email:email},(err,user) => {
       if(err) return res.status(500).send({message:'Error en la petición'});

       if(user){
           bcrypt.compare(params.password, user.password, (err, check) => {
               if(check){

                   if(params.gettoken){

                       return res.status(200).send({
                           token: jwt.createToken(user)
                       });

                   }else{
                       //devolver datos user
                       user.password = undefined;
                       return res.status(200).send({user});
                   }


               }else{
                   return res.status(404).send({message:'El usuario no se ha podido identificar'});
               }
           });
       }else{
           return res.status(404).send({message:'El usuario no se ha podido identificar'});
       }
    });
}

function getMyCounts(req,res){
    Follow.countDocuments({user:req.user.sub}).exec((err, countFollowing)=>{
        if(err) return res.status(500).send({message:'Error en la petición'});
        Follow.countDocuments({siguiendo:req.user.sub}).exec((err, countFollowed)=>{
            if(err) return res.status(500).send({message:'Error en la petición'});
            return res.status(200).send({countFollowing,countFollowed});
        })
    })
}

function getUser(req,res){
    var userId = req.params.id;

    
    User.findOne({_id:userId}).select('-password -role').exec((err, user) => {
       if(err) return res.status(500).send({message:'Error en la petición'});

       if(!user) return res.status(404).send({message:'Usuario no existe'});

        Follow.countDocuments({user:userId}).exec((err, countFollowing)=>{
            if(err) return res.status(500).send({message:'Error en la petición'});
            Follow.countDocuments({siguiendo:userId}).exec((err, countFollowed)=>{
                if(err) return res.status(500).send({message:'Error en la petición'});
                return res.status(200).send({user,countFollowing,countFollowed});
            })
        })

    });
}

function searchUser(req,res){

    User.find({$or: [
        {email: req.params.search},
        {nick: req.params.search},
        {name: req.params.search},
        {surname: req.params.search},
        {slug: req.params.search},
    ]}).select('-password -role').exec((err, user) =>{
        if(err) return res.status(500).send({message:'Error en la petición'});

       if(!user) return res.status(404).send({message:'Usuario no existe'});

       return res.status(200).send({user});
    });
}

function getUsers(req,res){
    // usuario del token req.user.sub

    var identity_user_id = req.user.sub;

    var page = 1;
    if(req.params.page){
        page = req.params.page;
    }

    var itemsPerPage = 5;

    User.find().sort('_id').paginate(page, itemsPerPage, (err,users, total) => {
        if(err) return res.status(500).send({message: 'Error en la petición'});

        if(!users) return res.status(404).send({message: 'No hay usuarios disponibles'});

        return res.status(200).send({
            users,
            total,
            pages: Math.ceil(total/itemsPerPage),
        });
    });
}

function updateUser(req,res){
    var userId = req.params.id;
    var update = req.body;

    // borrar propiedad password

    delete update.password;

    if(userId != req.user.sub){
        return res.status(500).send({message: 'No tienes permiso para actualizar el usuario'});
    }

    User.findByIdAndUpdate(userId, update, {new:true}, (err, userUpdated) =>{
        if(err) return res.status(500).send({message: 'Error en la petición'});

        if(!userUpdated) return res.status(404).send({message: 'No se ha podido actualizar'});

        return res.status(200).send({
            user: userUpdated,
        });
    });

}


// Subir archivos de imagen
function uploadImage(req,res){
    var userId = req.params.id;



    if(req.files){
       var file_path = req.files.image.path;

       var file_split = file_path.split('\\');

       var file_name = file_split[2];

       var ext_split = file_name.split('\.');

       var file_ext = ext_split[1];

        if(userId != req.user.sub){
            removeFileUploads(res, file_path, 'No tienes permiso para actualizar el usuario')
        }

       if(file_ext == 'png' || file_ext == 'jpg' || file_ext == 'jpeg' || file_ext == 'gif'){
            // Actualizar bd
           User.findByIdAndUpdate(userId, {image: file_name}, {new:true}, (err,userUpdated) => {
               if(err) return res.status(500).send({message: 'Error en la petición'});

               if(!userUpdated) return res.status(404).send({message: 'No se ha podido actualizar'});

               return res.status(200).send({
                   user: userUpdated,
               });
           });
       }else{
           removeFileUploads(res, file_path, 'Extension no valida');
       }
    }else{
        return res.status(200).send({message:'No se han subido imagenes'});
    }

}

function removeFileUploads(res, file_path, message){
    fs.unlink(file_path, (err) => {
        return res.status(200).send({message:message});
    });
}


function getImageFile(req,res){
    var image_file = req.params.imageFile;
    var path_file = './uploads/users/'+image_file;

    fs.exists(path_file, (exists) => {
        if(exists){
            res.sendFile(path.resolve(path_file));
        }else{
            res.status(200).send({message: 'No existe la imagen'});
        }
    })
}

module.exports = {
    home,
    getUser,
    uploadImage,
    getImageFile,
    updateUser,
    getUsers,
    saveUser,
    loginUser,
    searchUser,
    getMyCounts
};