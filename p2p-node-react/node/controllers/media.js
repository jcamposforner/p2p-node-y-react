'use strict'

var mongoosePaginate = require('mongoose-pagination');
var bcrypt = require('bcrypt-nodejs');
var fs = require('fs');
var path = require('path');
var moment = require('moment');
var slugify = require('slugify')




// Primera letra mayuscula para saber que es un modelo
var Media = require('../models/media');
var Votes = require('../models/votes');

var jwt = require('../services/jwt');



function home(req,res){
    res.status(200).send({
        message: 'Home express'
    });
}


function saveMedia(req,res){
    //Campos de POST
    var params = req.body;

    // Creamos nuevo usuario
    var media = new Media();


    if(params.title && params.description && params.type && params.slug && params.style){

        media.user = req.user.sub;
        media.type = params.type;
        media.media = null;
        media.title = params.title;
        media.description = params.description;
        media.slug = slugify(params.slug, { replacement: '-', remove: null, lower: true});
        media.activated = false;
        media.style = params.style;
        media.created_at = moment().unix();

        // Comprobar usuarios duplicados

        Media.find({ $or: [
            {title: media.title.toLowerCase()},
            {slug:media.slug.toLowerCase()}
        ]}).exec((err,medias) => {
            if(err) return res.status(500).send({message:'Error en la petición de usuarios'});

            if(medias && medias.length >= 1){
                return res.status(200).send({message: 'El video/audio ya existe'});
            }else{
                media.save((err,mediaStored) => {
                    if(err) return res.status(500).send({message:'Ha ocurrido un error al guardar el usuario'});

                    if(mediaStored){
                        res.status(200).send({media: mediaStored});
                    }else{
                        res.status(404).send({message:'No se ha registrado el usuario'});
                    }
                });
            }
        });
    }else{
        res.status(200).send({
           message: 'Rellena todos los datos',
        });
    }

}


async function getMyMedias(req,res){

    var userId = req.user.sub;

    var page = 1;
    if(req.params.page){
        page = req.params.page;
    }

    var itemsPerPage = 12;

    Media.find({user:userId,activated:true}).sort('-created_at').paginate(page, itemsPerPage, (err,medias, total) => {
        if(err) return res.status(500).send({message: 'Error en la petición'});

        if(!medias) return res.status(404).send({message: 'No hay usuarios disponibles'});

        return res.status(200).send({
            medias,
            total,
            pages: Math.ceil(total/itemsPerPage),
            currentPage:page,
        });
    });
    

}

function getMediasByUser(req,res){

    var userId = req.params.id;

    var page = 1;
    if(req.params.page){
        page = req.params.page;
    }

    var itemsPerPage = 12;

    Media.find({user:userId,activated:true}).sort('-created_at').populate('user','-__v -password -role').paginate(page, itemsPerPage, (err,medias, total) => {
        if(err) return res.status(500).send({message: 'Error en la petición'});

        if(!medias) return res.status(404).send({message: 'No hay usuarios disponibles'});

        return res.status(200).send({
            medias,
            total,
            pages: Math.ceil(total/itemsPerPage),
            currentPage:page,
        });
    });
    

}

function getMedia(req,res){
    var mediaSlug = req.params.slug;

    Media.find({slug:mediaSlug}).populate('user','-__v -password -role').exec((err,media) =>{
        if(err) return res.status(500).send({message:'Error en la petición'});

        if(!media) return res.status(404).send({message:'Media no existe'});

        Votes.find({media:media._id}).populate('user','-_id -__v -password -role').exec((err,votes)=>{
            if(err) return res.status(500).send({message:'Error en la petición'});
            return res.status(200).send({
                media,
                votes
            });  
        })
    });

}

function getMedias(req,res){
    // usuario del token req.user.sub

    var page = 1;
    if(req.params.page){
        page = req.params.page;
    }

    var itemsPerPage = 12;

    Media.find({activated:true}).sort('_id').populate('user','-_id -__v -password -role').paginate(page, itemsPerPage, (err,medias, total) => {
        if(err) return res.status(500).send({message: 'Error en la petición'});

        if(!medias) return res.status(404).send({message: 'No hay usuarios disponibles'});

        return res.status(200).send({
            medias,
            total,
            currentPage: page,
            pages: Math.ceil(total/itemsPerPage),
        });
    });
}

function getMediasByStyle(req,res){
    // usuario del token req.user.sub
    
    var page = 1;
    if(req.params.page){
        page = req.params.page;
    }

    var itemsPerPage = 12;

    Media.find({style:req.params.style, activated:true}).sort('_id').populate('user','-_id -__v -password -role').paginate(page, itemsPerPage, (err,medias, total) => {
        if(err) return res.status(500).send({message: 'Error en la petición'});

        if(!medias) return res.status(404).send({message: 'No hay usuarios disponibles'});

        return res.status(200).send({
            medias,
            total,
            currentPage: page,
            pages: Math.ceil(total/itemsPerPage),
        });
    });
}

// Subir archivos de imagen
function uploadMedia(req,res){
    var mediaId = req.params.id;



    // Consulta para ver si el media es del usuario que intenta subir un video/audio

    if(req.files){
       var file_path = req.files.media.path;

       var file_split = file_path.split('\\');

       var file_name = file_split[2];

       var ext_split = file_name.split('\.');

       var file_ext = ext_split[1];

        //if(userId != req.user.sub){
        //    removeFileUploads(res, file_path)
        //    return res.status(403).send({message:'No tienes permiso para actualizar el producto'});
        //}

        




       if(file_ext == 'mp3' || file_ext == 'mp4' ){
            // Actualizar bd
            
           Media.findByIdAndUpdate(mediaId, {media: file_name, user: req.user.sub, activated:true,}, {new:true}, (err,mediaUpdated) => {
               if(err) return res.status(500).send({message: 'Error en la petición'});

               if(!mediaUpdated) return res.status(404).send({message: 'No se ha podido actualizar'});

               return res.status(200).send({
                   media: mediaUpdated,
               });
           });
       }else{
            removeFileUploads(res, file_path);
            return res.status(403).send({message:'Extension no valida'});       
        }
    }else{
        return res.status(200).send({message:'No se han subido imagenes'});
    }

}

function uploadThumbmail(req,res){
    var mediaId = req.params.id;
    // Consulta para ver si el media es del usuario que intenta subir un video/audio

    if(req.files){
       var file_path = req.files.thumbmail.path;

       var file_split = file_path.split('\\');

       var file_name = file_split[2];

       var ext_split = file_name.split('\.');

       var file_ext = ext_split[1];

        //if(userId != req.user.sub){
        //    removeFileUploads(res, file_path)
        //    return res.status(403).send({message:'No tienes permiso para actualizar el producto'});
        //}

       if(file_ext == 'png' || file_ext == 'jpg' ){
            // Actualizar bd
            
           Media.findByIdAndUpdate(mediaId, {thumbmail: file_name, activated:true,}, {new:true}, (err,mediaUpdated) => {
               if(err) return res.status(500).send({message: 'Error en la petición'});

               if(!mediaUpdated) return res.status(404).send({message: 'No se ha podido actualizar'});

               return res.status(200).send({
                   media: mediaUpdated,
               });
           });
       }else{
            removeFileUploads(res, file_path);
            return res.status(403).send({message:'Extension no valida'});       
        }
    }else{
        return res.status(200).send({message:'No se han subido imagenes'});
    }

}

function removeFileUploads(res, file_path){
    fs.unlink(file_path, (err) => {
        
    });
}


function getMediaFile(req,res){
    var media_file = req.params.mediaFile;
    var path_file = './uploads/media/'+media_file;

    const stat = fs.statSync(path_file);
    const fileSize = stat.size;
    const range = req.headers.range;
    if (range) {
        const parts = range.replace(/bytes=/, "").split("-");
        
        const start = parseInt(parts[0], 10);
        const end = parts[1] 
        ? parseInt(parts[1], 10)
        : fileSize-1
        const chunksize = (end-start)+1 // Por si es 0 sumamos 1 

        const file = fs.createReadStream(path_file, {start, end})
        const head = {
            'Content-Range': `bytes ${start}-${end}/${fileSize}`, // Buffer
            'Accept-Ranges': 'bytes',
            'Content-Length': chunksize,
            'Content-Type': 'video/mp4',
        }

        res.writeHead(206, head);
        file.pipe(res);
    }else {
        const head = {
          'Content-Length': fileSize,
          'Content-Type': 'video/mp4',
        }
        res.writeHead(200, head)
        fs.createReadStream(path).pipe(res)
      }

    //fs.exists(path_file, (exists) => {
    //    if(exists){
    //        res.sendFile(path.resolve(path_file));
    //    }else{
    //        res.status(200).send({message: 'No existe el video/audio'});
    //    }
    //})
}


function getThumbmailFile(req,res){
    var image_file = req.params.mediaFile;
    var path_file = './uploads/thumbmails/'+image_file;

    fs.exists(path_file, (exists) => {
        if(exists){
            res.sendFile(path.resolve(path_file));
        }else{
            res.status(200).send({message: 'No existe la imagen'});
        }
    })
}

module.exports = {
    saveMedia,
    uploadMedia,
    getMedia,
    getMedias,
    getMediasByStyle,
    getMyMedias,
    getMediaFile,
    uploadThumbmail,
    getThumbmailFile,
    getMediasByUser
};
