'use strict'

var mongoosePaginate = require('mongoose-pagination');
var moment = require('moment');


// Primera letra mayuscula para saber que es un modelo
var Historial = require('../models/historial');


function saveHistorial(req,res){
    //Campos de POST
    var params = req.body;
    // Creamos nuevo usuario
    var historial = new Historial();

    if(params.media){

        historial.user = req.user.sub;
        historial.media = params.media;
        historial.created_at = moment().unix();
        
        Historial.countDocuments({$and: [
            {user: req.user.sub},
            {media:params.media}
        ]}).exec((err,vote)=>{
            if(err) return res.status(500).send({message:'Ha ocurrido un error al guardar el voto'});
            if(!vote > 0){
        
            historial.save((err,historialStored) => {
                if(err) return res.status(500).send({message:'Ha ocurrido un error al guardar el voto'});

                if(historialStored){
                    res.status(200).send({historial: historialStored});
                }else{
                    res.status(404).send({message:'No se ha registrado el historial'});
                }
            });
        }else{
            var update = {
                created_at: moment().unix(),
            };
            Historial.updateOne({$and:[
                {user:req.user.sub},
                {media:params.media}
            ]},update, {new:true}, (err, historialUpdated) =>{
                if(err) return res.status(500).send({message: 'Error en la petición'});
        
                if(!historialUpdated) return res.status(404).send({message: 'No se ha podido actualizar'});
        
                return res.status(200).send({
                    historial: historialUpdated,
                });
            });
        }
    })

    }else{
        res.status(200).send({
           message: 'Rellena todos los datos',
        });
    }

}

function getMyHistorial(req,res){

    var page = 1;
    if(req.params.page){
        page = req.params.page;
    }

    var itemsPerPage = 12;

    Historial.find({user:req.user.sub}).select('-_id -__v').sort('-created_at').populate('media','-__v').deepPopulate('media.user', '-user._id -__v -password -role').paginate(page, itemsPerPage, (err,videos, total) => {
        if(err) return res.status(500).send({message: 'Error en la petición'});

        if(!videos) return res.status(404).send({message: 'No hay videos disponibles'});
        
        videos.forEach(video => {
            video.media.user.password = undefined;
            video.media.user.role = undefined
        });

        return res.status(200).send({
            videos,
            currentPage:page,
            total,
            pages: Math.ceil(total/itemsPerPage),
        });
    });
}

module.exports = {
    saveHistorial,
    getMyHistorial
};