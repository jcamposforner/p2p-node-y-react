'use strict'

var express = require('express');
var VotesController = require('../controllers/votes');

var md_auth = require('../middlewares/authenticated');
var api = express.Router();


api.get('/my-likes',md_auth.ensureAuth, VotesController.getMyLikes);
api.post('/add-like',md_auth.ensureAuth, VotesController.saveVotes);


module.exports = api;