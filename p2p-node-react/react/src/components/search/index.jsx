import React,{Component} from 'react';
import PropTypes, { func } from 'prop-types';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';
import { urlApi } from '../../env';
import Button from '@material-ui/core/Button';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {follow,unfollow} from "../../actions";
import axios from 'axios';
import { Link } from 'react-router-dom';


const styles = theme => ({
    main: {
      width: 'auto',
      display: 'block', // Fix IE 11 issue.
      marginLeft: theme.spacing.unit * 3,
      marginRight: theme.spacing.unit * 3,
      [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
        width: 400,
        marginLeft: 'auto',
        marginRight: 'auto',
      },
    },
    button: {
        margin: theme.spacing.unit,
    },
    paper: {
      marginTop: theme.spacing.unit * 8,
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
    },
    avatar: {
      margin: theme.spacing.unit,
      backgroundColor: theme.palette.secondary.main,
    },
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing.unit,
    },
    submit: {
      marginTop: theme.spacing.unit * 3,
    },
    smallAvatar: {
      margin: 10,
      width: 100,
      height: 100,
    },
  });


class Search extends Component {

    constructor(props){
        super(props);

        this.state = {
            following: [],
            follows: [],
        }
     
    }

    componentWillMount(){
        axios.get(`${urlApi}get-all-follows`, {headers: {
            'Content-Type':'application/json',
            Authorization: JSON.parse(localStorage.getItem('token')),
        }})
            .then((res) =>{

                let followingObject = res.data.following;
                

                this.setState({
                    following: followingObject.map((val) => { return val.siguiendo }),
                    follows: res.data.follows.map((val) => { return val.user })
                })
            },(err) =>{
                console.log(err)
            }
        )
    }


    followUser(userId){
        
        this.props.follow(userId);
    }
    unfollowUser(userId){
        
        this.props.unfollow(userId);
    }

    render (){
        const { classes } = this.props;
        const { users,query } = this.props.location.state;
        const { following, follows } = this.state;
        console.log(users)
        return (
            <main>
                <div style={{marginTop:'2rem'}} className="container">
                        <Typography style={{marginBottom:'2rem'}} component="h2" variant="h3" align="center" color="textSecondary">
                            Resultados de {query} ({users.length})
                        </Typography>
                        {
                            users.length > 0 ?
                            
                             <div>
                             
                             
                             { 
                            users.map((user) =>
                                <div style={{borderBottom:'1px solid #d4d4d4'}} className="columns" key={user._id}>
                                    
                                    <div className="column is-12">
                                        <article className="media">
                                            <Link to={"/perfil/"+user._id}>
                                                <figure className="media-left">
                                                    
                                                        <Avatar alt={user._id} src={urlApi+"get-image-user/"+user.image} className={classes.smallAvatar} />
                                                    
                                                </figure>
                                            </Link>
                                            <div className="media-content">
                                                <Link to={"/perfil/"+user._id} className="search-links">
                                                    <div className="content">
                                                    <p>
                                                    {/* Falta link de perfil del usuario */}
                                                        <strong>{user.name} {user.surname}</strong> <small>@{user.nick}</small> 
                                                        <br />
                                                        a
                                                    </p>
                                                    </div>
                                                </Link>
                                                <nav className="level is-mobile">
                                                <div className="level-left">
                                                    <p style={{cursor:'pointer'}} className="level-item">
                                                        <span className="icon is-small"><i className="fas fa-heart"></i></span>
                                                    </p>
                                                </div>
                                                </nav>
                                            </div>
                                            <div className="media-right">
                                                <Button onClick={() => this.followUser(user._id)} variant="contained" color="primary" className={classes.button}>
                                                    Seguir
                                                </Button>
                                            {
                                                
                                                following.map((follow, idx)=>
                                                    <span>{
                                                        follow._id.indexOf(user._id) ?
                                                        null
                                                        :
                                                        <span>
                                                            <Button onClick={() => this.unfollowUser(user._id)} variant="contained" color="secondary" className={classes.button}>
                                                                Dejar de seguir
                                                            </Button>
                                                        </span>
                                                        }</span>
                                                )
                                            }
                                                
                                                
                                            </div>
                                        </article>
                                    </div>

                                </div>
                            
                            )
                        }
                             
                             </div>
                            
                            
                            :
                            
                            <Typography component="h2" variant="h5" align="center" color="textSecondary">
                                No se han encontrado resultados
                            </Typography>
                        }
                        
                </div>
            </main>
        );
    };
}
Search.propTypes = {
    classes: PropTypes.object.isRequired,
  };

function mapStateToProps(state) {
    
    return {
        payload: state.ReducerFollow,
    }
}
function matchDispatchToProps(dispatch){
    return bindActionCreators({follow:follow,unfollow:unfollow}, dispatch);
}

export default 
    connect(mapStateToProps,matchDispatchToProps)(withStyles(styles)(Search));
