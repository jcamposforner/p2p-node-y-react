import React, { Component } from 'react';
import PropTypes, { func } from 'prop-types';
import MediaHandler from '../../services/MediaHandler';
import withStyles from '@material-ui/core/styles/withStyles';
import openSocket from 'socket.io-client';

const socket = openSocket('http://localhost:4000/stream')

const styles = theme => ({
  main: {
    width: 'auto',
    display: 'block',
    marginTop:theme.spacing.unit * 2,
  },
});

class StreamingStart extends Component {
  constructor(props){
    super(props)
    
    this.state = {
      paused: false,
      signalData: {},
    }
    this.mediaHandler = new MediaHandler;
  }
  componentWillMount(){
    
    socket.on("stream",(image) => {
      console.log(image)

      var img = document.getElementById("play")
      img.src = image
      var log = document.getElementById("logger");
      log.innerHTML = image
    })


  }


  
  render() {
    const { classes } = this.props;
    const { paused } = this.state;
    return (
      <main className={classes.main+" centered"}>
        <div className="container">
          <div className="columns">
            <img id="play"></img>
            <div id="logger"></div>
          </div>
          
        </div>
      </main>
    );
  }
}

StreamingStart.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(StreamingStart)
