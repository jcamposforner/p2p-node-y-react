import React,{Component} from 'react';
import { Player, ControlBar } from 'video-react';
import axios from 'axios';


const urlApi = 'http://localhost:3800/api/';



class VideoPlayer extends Component {

    constructor(props){
        super(props)

    }
    
    clicked(){
        const params = {
            media: this.props.mediaId
        }
        axios.post(`${urlApi}add-historial`,params, {
            headers : {
                'Content-Type':'application/json',
                'Authorization': JSON.parse(localStorage.getItem('token')),
            }
        }).then(
            (res) => {
                this.setState({
                    type: null,
                    created: true,
                    loading: false,
                })
            },(err) =>{
                console.log(err)
            }
        )
    }
    render (){
        const {media, thumbmail} = this.props
        return (
            <div onClick={this.clicked.bind(this)}>
                <Player 
                    poster={thumbmail}
                    ref="player">
                    <source src={media} />
                    <ControlBar autoHide={true} />
                </Player>
            </div>
        );
    };
}

export default VideoPlayer;