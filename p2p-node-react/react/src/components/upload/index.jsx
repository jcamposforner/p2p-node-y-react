import React,{Component} from 'react';
import PropTypes from 'prop-types';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import LockIcon from '@material-ui/icons/LockOutlined';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';
import { withRouter } from 'react-router-dom';
import axios from 'axios';
import { urlApi } from '../../env'
import CircularProgress from '@material-ui/core/CircularProgress';

const styles = theme => ({
    main: {
      width: 'auto',
      display: 'block', // Fix IE 11 issue.
      marginLeft: theme.spacing.unit * 3,
      marginRight: theme.spacing.unit * 3,
      [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
        width: 400,
        marginLeft: 'auto',
        marginRight: 'auto',
      },
    },
    paper: {
      marginTop: theme.spacing.unit * 8,
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
    },
    avatar: {
      margin: theme.spacing.unit,
      backgroundColor: theme.palette.secondary.main,
    },
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing.unit,
    },
    submit: {
      marginTop: theme.spacing.unit * 3,
    },
    progress: {
        margin: theme.spacing.unit * 2,
      },
  });


class UploadMedia extends Component {

    constructor(props){
        super(props)
        this.state = {
            loading:false,
            type:'',
            created:false,
            token:JSON.parse(localStorage.getItem('token')),
        };
    }

    handleSubmit(e) {
        e.preventDefault();
        const formData = {
            title: document.getElementById("title").value,
            description: document.getElementById("description").value,
            slug: document.getElementById("slug").value,
            style: document.getElementById("style").value,
            type: document.getElementById("type").value,
        }
        console.log(this.state.token)
        this.setState({
            loading: true,
            type:false
        })
        axios.post(`${urlApi}upload-media`,formData, {
             headers : {
                'Content-Type':'application/json',
                'Authorization': this.state.token
                }
            }).then(
                (res) => {
                    if(!res.data.media){
                        console.log('No se ha creado')
                    }else{
                        this.fileUploadHandler(res.data.media._id);
                        this.fileUploadHandlerThumbmail(res.data.media._id);
                    }
                    
                },(err) =>{
                    console.log(err)
                }
        )

    }

    fileUploadHandler = (mediaId) => {
        const fd = new FormData();
        fd.append('media', this.state.selectedFile, this.state.selectedFile.name);
        axios.post(`${urlApi}upload-video/${mediaId}`,fd, {
            headers : {
               'Content-Type':'application/json',
               'Authorization': this.state.token
               }
           }).then(
               (res) => {
                   this.setState({
                       type: null,
                       created: true,
                       loading: false,
                   })
               },(err) =>{
                   console.log(err)
               }
       )
    }

    fileUploadHandlerThumbmail = (mediaId) => {
        const fd = new FormData();
        if(this.state.selectedFileThumbmail){
            fd.append('thumbmail', this.state.selectedFileThumbmail, this.state.selectedFileThumbmail.name);
            axios.post(`${urlApi}upload-thumbmail/${mediaId}`,fd, {
                headers : {
                'Content-Type':'application/json',
                'Authorization': this.state.token
                }
            }).then(
                (res) => {
                    this.setState({
                        type: null,
                        created: true,
                        loading: false,
                    })
                },(err) =>{
                    console.log(err)
                }
        )
        }else{
            return false;
        }
    }

    

    setTypeAudio(){
        this.setState({
            created: null,
            loading: false,
            type: 'Audio',
        });
    }
    setTypeVideo(){
        this.setState({
            created: null,
            loading: false,
            type: 'Video',
        });
    }


    fileSelectedHandler = event => {
        this.setState({
            selectedFile: event.target.files[0]
        })
    }
    fileSelectedHandlerThumbmail = event => {
        this.setState({
            selectedFileThumbmail: event.target.files[0]
        })
    }
    

    render (){
        const { classes } = this.props;
        const { type,created,loading } = this.state;

        return (
            <main className={classes.main+" centered"}>
            <CssBaseline />
            <Paper className={classes.paper}>
                <Avatar className={classes.avatar}>
                <LockIcon />
                </Avatar>
                <Typography component="h1" variant="h5">
                    Elige que quieres subir
                </Typography>
                <hr />
                <div className="columns media-upload">
                    <div onClick={this.setTypeAudio.bind(this)} className="column is-6">
                        <i className="fa fa-file-audio" aria-hidden="true"></i>
                    </div>
                    <div onClick={this.setTypeVideo.bind(this)} className="column is-6">
                        <i className="fas fa-file-video"></i>
                    </div>
                </div>
                { type && (
                    <form onSubmit={this.handleSubmit.bind(this)} className={classes.form}>
                        <FormControl margin="normal" required fullWidth>
                            <InputLabel htmlFor="title">Titulo</InputLabel>
                            <Input id="title" name="title" autoComplete="title" autoFocus />
                        </FormControl>
                        <FormControl margin="normal" required fullWidth>
                            <InputLabel htmlFor="description">Descripción</InputLabel>
                            <Input id="description" name="description" autoComplete="description" />
                        </FormControl>
                        <FormControl margin="normal" required fullWidth>
                            <InputLabel htmlFor="slug">Slug</InputLabel>
                            <Input id="slug" name="slug" autoComplete="slug" />
                        </FormControl>
                        <FormControl margin="normal" required fullWidth>
                            <InputLabel htmlFor="style">Estilo</InputLabel>
                            <Input id="style" name="style" autoComplete="style" />
                        </FormControl>
                        <input name="type" id="type" type="hidden" value={this.state.type} />
                        
                        <p>Video</p>
                        <input onChange={this.fileSelectedHandler.bind(this)} type="file" required />

                        {
                            type == 'Video' ? 
                                <div>
                                    <p>Miniatura</p>
                                    <input onChange={this.fileSelectedHandlerThumbmail.bind(this)} type="file" />
                                </div>
                            :null
                        }
                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                            className={classes.submit}
                        >
                        Siguiente
                    </Button>
                </form>
                )}
                { created && (

                    <div className="columns">
                        <div className="column is-12 has-text-centered">
                            <Typography component="h1" variant="h6">
                                Se ha subido con exito
                            </Typography>
                        </div>
                    </div>

                )}
                { loading && (
                    <div>
                        <CircularProgress className={classes.progress} />
                    </div>
                )}

                </Paper>
            </main>
        );
    };
}
UploadMedia.propTypes = {
    classes: PropTypes.object.isRequired,
  };


export default 
    withStyles(styles)(
    (withRouter(UploadMedia))
);