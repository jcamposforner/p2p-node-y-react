import {combineReducers} from 'redux';
import ReducerUser from './reducer-user';
import ReducerFollow from './reducer-follow';

const allReducers = combineReducers({
    ReducerUser: ReducerUser,
    ReducerFollow: ReducerFollow,
});

export default allReducers;