import React,{Component} from 'react';
import Avatar from '@material-ui/core/Avatar';
import { withStyles } from '@material-ui/core/styles';
import { urlApi } from '../../env';
import axios from 'axios';
import Fade from '@material-ui/core/Fade';
import Typography from '@material-ui/core/Typography';

const styles = theme => ({
    avatar: {
        margin: 10,
      },
      bigAvatar: {
        margin: 10,
        width: 60,
        height: 60,
      },
      success: {
          padding:10,
          textAlign:'center'
      }
  });

class FormComments extends Component {

    constructor(props){
        super(props)

        this.state = {
            published:false,
        }
    }
        

    
    submitComment(e){
        const data = {
            body: document.getElementById("commentArea").value,
        }

        e.preventDefault();
        axios.post(`${urlApi}add-comment/${this.props.mediaId}`,data, {
             headers : {
                'Content-Type':'application/json',
                'Authorization': this.props.token
                }
            }).then(
                (res) => {
                    if(!res.data.comment){
                        console.log('No se ha creado')
                    }else{
                        this.setState({
                            published: true,
                        })
                    }
                    
                },(err) =>{
                    console.log(err)
                }
        )
        this.refs.comments.value = "";
        setTimeout(()=>{ this.setState({published:false}); }, 2500);
    }
    
    render (){      
        const { classes,identity } = this.props;
        const { published } = this.state;

        return (
            <div>
                
                <Fade timeout={1000} in={published}>
                    <div className="columns">
                        <div className="column">
                            <div style={{backgroundColor:'#00d1b2'}} className={classes.success}>
                                <Typography component="h1" variant="h5">
                                   ¡Tu comentario se ha publicado con exito!
                                </Typography>
                            </div>
                        </div>
                    </div>
                </Fade>
                <article className="media">
                    <figure className="media-left">
                        <Avatar alt={identity._id} src={urlApi+"get-image-user/"+identity.image} className={classes.avatar} />
                    </figure>
                    <div className="media-content">
                        <div className="field">
                            <p className="control">
                                <textarea ref="comments" id="commentArea" className="textarea" placeholder="Añade un comentario..."></textarea>
                            </p>
                        </div>
                        <nav className="level">
                            <div className="level-left">
                                <div className="level-item">
                                    <button onClick={this.submitComment.bind(this)} className="button is-info">Enviar</button>
                                </div>
                            </div>
                        </nav>
                    </div>
                </article>
            </div>
        );
    };
}

export default withStyles(styles)(FormComments);