'use strict'

var express = require('express');
var bodyParser = require('body-parser');
var moment = require('moment');

var app = express();

var ss = require('socket.io-stream');

var http = require('http').Server(app);
var io = require('socket.io')(http)
http.listen(4000) // Puerto del chat

var clients = [];

io.of('/stream').on('connection',(socket) => {
    socket.on('streaming-client', (stream) => {
        socket.broadcast.emit('stream',stream)
    })
})

io.on('connection', (socket) => {
    socket.on("user", (userId)=>{
        const data = {
            userId: userId,
            socketId: socket.id
        };
        clients.push(data)
    })
    socket.on('disconnect', (socket) => {
        for(let i=0; i < clients.length;i++){
            clients.splice(clients[i].socketId.indexOf(socket.id), 1);
        }
    });

    socket.on("new-message", (msg,from,_id) => {
        var data = {
            created_at:moment().unix(),
            msg: msg,
            from:from,
            _id:_id,
        }
        io.emit("receive-message", data)
    })
});


// cargar rutas

var user_routes = require('./routes/user');
var media_routes = require('./routes/media');
var comment_routes = require('./routes/comments');
var follow_routes = require('./routes/siguiendo');
var likes_routes = require('./routes/votes');
var historial_routes = require('./routes/historial');

// middlewares ( metodo que se ejecuta antes del controlador )

// Convertir a JSON el body
app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());

// cors
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');


    next();
});

//rutas

app.use('/api', user_routes);
app.use('/api', media_routes);
app.use('/api', comment_routes);
app.use('/api', follow_routes);
app.use('/api', likes_routes);
app.use('/api', historial_routes);

// exportar conf

module.exports = app,io;