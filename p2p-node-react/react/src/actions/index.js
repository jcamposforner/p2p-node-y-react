import axios from 'axios';

const apiUrl = 'http://localhost:3800/api/';
const identity = JSON.parse(localStorage.getItem('identity'));
const token = JSON.parse(localStorage.getItem('token'));


export const userLogged = () => {
    console.log('test0')
    return {
        type: "USER_LOGGED",
        payload: true,
    }
}


export function storeFollow(userId){
    const data = {
        id: userId,
    }
    return function(dispatch){
        

        return axios.post(`${apiUrl}add-follow`,data,{
            headers: {
                'Content-Type':'application/json',
                'Authorization': token
            }
        }).then((res) =>{
            dispatch(follow(data, res.data))
        })

    }
}

export const follow = (data,res) => {

    
    return {
        type: "FOLLOW_USER",
        payload: res,
    }
    
    
}



export const unfollow = (userId) => {
    const data = {
        id: userId,
    }
    axios.post(`${apiUrl}unfollow`,data, {
        headers : {
            'Content-Type':'application/json',
            'Authorization': token
        }
    }).then(
        (res) => {
            console.log(res.data)
        },(err) =>{
            console.log(err)
        }
    )

    return {
        type: "UNFOLLOW_USER",
        payload: 'unfollow success',
    }
}