import React,{Component} from 'react';
import { Player, ControlBar } from 'video-react';



class VideoPlayer extends Component {

    constructor(props){
        super(props)

    }
    
    clicked(){
        console.log(this.props.media)
    }
    render (){
        const {media, thumbmail} = this.props
        return (
            <div onClick={this.clicked.bind(this)}>
                <Player 
                    poster={thumbmail}
                    ref="player">
                    <source src={media} />
                    <ControlBar autoHide={true} />
                </Player>
            </div>
        );
    };
}

export default VideoPlayer;