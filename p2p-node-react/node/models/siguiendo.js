'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var FollowSchema = Schema({
    created_at: String,
    siguiendo: { type: Schema.ObjectId, ref: 'User' },
    user: { type: Schema.ObjectId, ref: 'User' },
});


module.exports = mongoose.model('Follow', FollowSchema);