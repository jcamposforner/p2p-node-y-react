import React, { Component } from 'react';
import PropTypes, { func } from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import AccountCircle from '@material-ui/icons/AccountCircle';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import {connect} from 'react-redux';
import { Link } from 'react-router-dom';
import InputBase from '@material-ui/core/InputBase';
import SearchIcon from '@material-ui/icons/Search';
import { fade } from '@material-ui/core/styles/colorManipulator';
import { withRouter } from 'react-router-dom';
import { urlApi } from '../../env'
import axios from 'axios';
import MediaHandler from '../../services/MediaHandler'

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    marginRight: theme.spacing.unit * 2,
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing.unit * 3,
      width: 'auto',
    },
  },
  searchIcon: {
    width: theme.spacing.unit * 9,
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputRoot: {
    color: 'inherit',
    width: '100%',
  },
  inputInput: {
    paddingTop: theme.spacing.unit,
    paddingRight: theme.spacing.unit,
    paddingBottom: theme.spacing.unit,
    paddingLeft: theme.spacing.unit * 10,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: 200,
    },
  },
});

class NavBar extends Component {
  state = {
    auth: (localStorage.getItem('auth')) ? true : false,
    anchorEl: null,
    users:false,
    user:[],
  };
  


  handleMenu = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleClose = () => {
    this.setState({ anchorEl: null });
  };

  logOut(){
    localStorage.removeItem('token');
    localStorage.removeItem('identity');
    localStorage.removeItem('auth');
    document.location.reload(true);
  }

  hideSearch(){
    this.setState({
      users:false,
      user: [],
    })
  }
  handlePress(event){
    var search = document.getElementById("search").value;
    var code = event.keyCode || event.which;
    
    if(code === 13){
      axios.get(`${urlApi}search-user/${search}`, {headers:{
        'Content-Type':'application/json',
        'Authorization': JSON.parse(localStorage.getItem('token')),
      }}).then(
        (res) =>{
          if(!res.data.user){
            this.setState({
              users:false,
              user: [],
            })
          }else{
            this.props.history.push({
              pathname: '/search',
              state: { 
                users: res.data.user,
                following: res.data.following,
                query: search
              },
            });
          }
        },(err) =>{
          console.log(err)
        }
      )
    }
    
  }
  


  render() {
    const { classes } = this.props;
    const open = Boolean(anchorEl);
    const { auth, anchorEl, users, user } = this.state;
    console.log(auth)
    return (
      <div>
        <AppBar position="static">
          <Toolbar>
            <IconButton className={classes.menuButton} color="inherit" aria-label="Menu">
              <MenuIcon />
            </IconButton>
            <Typography className={classes.menuButton} variant="h6" color="inherit">
              <Link to="/">Inicio</Link>
            </Typography>
            <Typography className={classes.menuButton} variant="h6" color="inherit">
              <Link to="/stream">Stream</Link>
            </Typography>
            <Typography className={classes.menuButton} variant="h6" color="inherit">
              <Link to="/coleccion">Colección</Link>
            </Typography>
            <div className={classes.search+" "+classes.grow}>
              <div className={classes.searchIcon}>
                <SearchIcon />
              </div>
                <InputBase
                  id="search"
                  onKeyPress={this.handlePress.bind(this)}
                  placeholder="Buscar..."
                  classes={{
                    root: classes.inputRoot,
                    input: classes.inputInput,
                  }}
                />
              
            </div>
            
            {
              this.props.auth ? (
                <div>
                <Link to="/upload">
                <IconButton
                  color="inherit"
                >
                  <i className="fa fa-upload" aria-hidden="true"></i>
                

                </IconButton>
                </Link>
                <IconButton
                  aria-owns={open ? 'menu-appbar' : undefined}
                  aria-haspopup="true"
                  onClick={this.handleMenu}
                  color="inherit"
                >
                  <AccountCircle />
                </IconButton>
                <Menu
                  id="menu-appbar"
                  anchorEl={anchorEl}
                  anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                  }}
                  transformOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                  }}
                  open={Boolean(anchorEl)}
                  onClose={this.handleClose}
                >
                  <Link to="/mi-perfil">
                    <MenuItem onClick={this.handleClose}>Perfil</MenuItem>
                  </Link>
                  <Link to="/my-account">
                    <MenuItem onClick={this.handleClose}>Mi cuenta</MenuItem>
                  </Link>
                  <MenuItem onClick={this.logOut}>Cerrar Sesión</MenuItem>
                  
                </Menu>
                </div>
              )
            :null}
            {auth ? (
              <div>
              <Link to="/upload">
              <IconButton
                  color="inherit"
                >
                  
                
                  <i className="fa fa-upload" aria-hidden="true"></i>
                  

                </IconButton>
                </Link>
                <IconButton
                  aria-owns={open ? 'menu-appbar' : undefined}
                  aria-haspopup="true"
                  onClick={this.handleMenu}
                  color="inherit"
                >
                  <AccountCircle />
                  
                </IconButton>
                <Menu
                  id="menu-appbar"
                  anchorEl={anchorEl}
                  anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                  }}
                  transformOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                  }}
                  open={Boolean(anchorEl)}
                  onClose={this.handleClose}
                >
                  <Link to="/mi-perfil">
                    <MenuItem onClick={this.handleClose}>Perfil</MenuItem>
                  </Link>
                  <Link to="/my-account">
                    <MenuItem onClick={this.handleClose}>Mi cuenta</MenuItem>
                  </Link>
                  <MenuItem onClick={this.logOut}>Cerrar Sesión</MenuItem>
                </Menu>
              </div>
            ):<div>
                <Link to="/registro">
                  <IconButton
                    color="inherit"
                  >
                    <i className="fa fa-registered" aria-hidden="true"></i>
                    
                  </IconButton>
                </Link>
                <Link to="/login">
                  <IconButton
                    color="inherit"
                  >
                    <i className="fa fa-sign-in-alt" aria-hidden="true"></i>
                  </IconButton>
                </Link>
              
                </div>}
          </Toolbar>
        </AppBar>
      </div>
    );
  }
}
NavBar.propTypes = {
  classes: PropTypes.object.isRequired,
};


function mapStateToProps(state) {
    console.log(state)
    return {
        auth: state.ReducerUser,
    }
    
}

export default withStyles(styles)(
  connect(mapStateToProps)(withRouter(NavBar))
);
