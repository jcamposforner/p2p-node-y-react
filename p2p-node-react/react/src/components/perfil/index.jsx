import React,{Component} from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { urlApi } from '../../env'
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import axios from 'axios';
import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import FavoriteIcon from '@material-ui/icons/Favorite';
import AudioPlayer from "react-h5-audio-player";
import VideoPlayer from './VideoPlayer';
import { Link } from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


const apiUrl = 'http://localhost:3800/api/';
const styles = theme => ({
    main: {
      width: 'auto',
      display: 'block', // Fix IE 11 issue.
      marginLeft: theme.spacing.unit * 3,
      marginTop: theme.spacing.unit *3,
      marginRight: theme.spacing.unit * 3,
      [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
        marginLeft: theme.spacing.unit * 3,
        marginRight: theme.spacing.unit * 3,
      },
    },
    paper: {
      marginTop: theme.spacing.unit * 8,
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
    },
    avatar: {
      margin: theme.spacing.unit,
      backgroundColor: theme.palette.secondary.main,
    },
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing.unit,
    },
    submit: {
      marginTop: theme.spacing.unit * 3,
    },
    avatar: {
        margin: 10,
      },
      bigAvatar: {
        margin: 40,
        width: 240,
        height: 240,
      },
  });


class Perfil extends Component {

    constructor(props){
        super(props)
        this.state = {
            type:'',
            identity:JSON.parse(localStorage.getItem('identity')),
            token:JSON.parse(localStorage.getItem('token')),
            medias:[],
            following:0,
            followed:0,
        };
    }


    async componentWillReceiveProps(nextProps){
        window.scrollTo(0, 0);

        var pageUrl = nextProps.match.params.page;
        if(!pageUrl){
            pageUrl = 1;
        }

        if(nextProps.match.page){

            axios.get(`${urlApi}my-counts`, {
                headers : {
                   'Content-Type':'application/json',
                   'Authorization': this.state.token
                   }
               }).then(
                (res)=>{
                    this.setState({
                        following: res.data.countFollowing,
                        followed: res.data.countFollowed,
                    })
                },(err)=>{
                    console.log(err)
                }
            )

            await axios.get(`${urlApi}my-medias/${pageUrl}`, {
                headers : {
                   'Content-Type':'application/json',
                   'Authorization': this.state.token
                   }
               }).then(
                   (res) => {
                       this.setState({
                           medias: res.data.medias,
                           currentPage: res.data.currentPage,
                           total: res.data.total,
                           pages: res.data.pages,
                       })
                   },(err) =>{
                       console.log(err)
                   }
           )
        }else{

            axios.get(`${urlApi}my-counts`, {
                headers : {
                   'Content-Type':'application/json',
                   'Authorization': this.state.token
                   }
               }).then(
                (res)=>{
                    this.setState({
                        following: res.data.countFollowing,
                        followed: res.data.countFollowed,
                    })
                },(err)=>{
                    console.log(err)
                }
            )

            await axios.get(`${urlApi}my-medias/${pageUrl}`, {
                headers : {
                   'Content-Type':'application/json',
                   'Authorization': this.state.token
                   }
               }).then(
                   (res) => {
                       this.setState({
                           medias: res.data.medias,
                           currentPage: res.data.currentPage,
                           total: res.data.total,
                           pages: res.data.pages,
                       })
                   },(err) =>{
                       console.log(err)
                   }
           )
        }

    }

    async componentWillMount(){
        window.scrollTo(0, 0);

        var pageUrl = this.props.match.params.page;
        if(!pageUrl){
            pageUrl = 1;
        }


        axios.get(`${urlApi}my-counts`, {
            headers : {
               'Content-Type':'application/json',
               'Authorization': this.state.token
               }
           }).then(
            (res)=>{
                
                this.setState({
                    following: res.data.countFollowing,
                    followed: res.data.countFollowed,
                })
            },(err)=>{
                console.log(err)
            }
        )

        await axios.get(`${urlApi}my-medias/${pageUrl}`, {
            headers : {
               'Content-Type':'application/json',
               'Authorization': this.state.token
               }
           }).then(
               (res) => {
                   this.setState({
                       medias: res.data.medias,
                       currentPage: res.data.currentPage,
                       total: res.data.total,
                       pages: res.data.pages,
                   })
               },(err) =>{
                   console.log(err)
               }
       )
    }

    likeMedia(mediaId){
        
        const data = {
            media: mediaId,
        }
        axios.post(`${apiUrl}add-like`,data,{
            headers: {
                'Content-Type':'application/json',
                'Authorization': JSON.parse(localStorage.getItem('token'))
            }
        }).then((res) =>{
            
            if(res.data.voto){
                this.notifyLike('Has votado con exito!');
            }else{
                this.notifyLike(res.data.message,'error');
            }
        },(err)=>{
            console.log(err)
        })
    }

    notifyLike = (message,tipo) => {
        if(tipo == 'error'){
            toast.error(message)
        }else{
            toast.info(message)
        }
    };

    createPaginator(){
        var paginator = [];
        var url=[];
        url.push(this.props.location.pathname.split('/'));

        var StringToInt = parseInt(this.state.currentPage,10);
        var PrevPage = StringToInt-1;
        var NextPage = StringToInt+1;

            if(this.state.currentPage != 1){
                paginator.push(<Link key={this.state.PrevPage} className="pagination-previous" to={"/"+url[0][1]+"/"+PrevPage}>Anterior</Link>);
            }
            if(this.state.pages != this.state.currentPage){
                paginator.push(<Link key={this.state.NextPage} className="pagination-previous" to={"/"+url[0][1]+"/"+NextPage}>Siguiente</Link>)
            }
        return paginator;
    }

    createPages(){
        var totalPages = [];
        var url=[];
        url.push(this.props.location.pathname.split('/'));
        for (let i = 1; i < this.state.pages+1; i++){
            if(i == this.state.currentPage){
                totalPages.push(
                    <li>
                        <Link key={i} to={"/"+url[0][1]+"/"+i} className="pagination-link is-current" aria-current="page" aria-label="Current Page">{i}</Link>
                    </li>
                );
            }else{
                totalPages.push(
                    <li>
                        <Link key={i} to={"/"+url[0][1]+"/"+i} className="pagination-link" aria-current="page" aria-label={"Goto page "+i}>{i}</Link>
                    </li>
                );
            }
        }
        return totalPages;
    }
    
    render (){
        const { classes } = this.props;
        const { identity,medias,total,followed,following } = this.state;

        const rows = [...Array( Math.ceil(medias.length / 3) )];

        const productRows = rows.map( (row, idx) => medias.slice(idx * 3, idx * 3 + 3) );
        const content = productRows.map((row, idx) => (
            <div className="columns" key={idx}>
                { row.map( media =>
                    <div key={media._id} className="column is-4">
                            <div className="card profile-card">
                                <div className="card-image">

                                        {
                                        media.type == 'Video' ? 
                                        
                                        <VideoPlayer mediaId={media._id} thumbmail={urlApi+"get-thumbmail/"+media.thumbmail} media={urlApi+"get-media/"+media.media}></VideoPlayer>
                                        
                                        :
                                        <div>
                                        <AudioPlayer
                                            controls
                                            src={urlApi+"get-media/"+media.media}
                                            onPlay={e => console.log("onPlay")}
                                        />
                                        </div>
                                        }
                                        

                                </div>
                                <div className="card-content">
                                    <Link className="link-profile" to={`/media/${media.slug}`}>
                                        <div className="content">
                                            Titulo: <b> {media.title}</b>
                                            <br/>
                                            Autor: <b>{identity.nick}</b>
                                        </div>
                                    </Link>
                                </div>
                                {media.type == 'Video' ? 
                                    <div>
                                        <BottomNavigation>
                                            <BottomNavigationAction onClick={()=>this.likeMedia(media._id)} label="Favorites" icon={<FavoriteIcon />} />
                                        </BottomNavigation>
                                    </div> 
                                    :
                                    <div className="like-profile">
                                        <BottomNavigation>
                                            <BottomNavigationAction onClick={()=>this.likeMedia(media._id)} label="Favorites" icon={<FavoriteIcon />} />
                                        </BottomNavigation>
                                    </div>
                                }
                            </div>
                    </div> )}
            </div> )
        );



        return (
            <div>
                <div className="columns">
                    <ToastContainer />
                    <div style={{backgroundImage: "url(/uploads/fondo-degradado.jpg)"}} className="column backgroundImage is-12">
                    
                        <div className="columns">
                            <div className="column is-4">
                                
                                <Avatar alt={this.state.identity._id} src={urlApi+"get-image-user/"+this.state.identity.image} className={classes.bigAvatar} />
                                
                            </div>

                            <div className="column is-4 flexbox">
                            <br></br><br></br><br></br><br></br>
                                <Typography component="h2" variant="h3" align="center" color="textPrimary">
                                    {this.state.identity.nick}
                                </Typography>
                                <Typography component="h2" variant="h5" align="center" color="inherit">
                                    {this.state.identity.name} {this.state.identity.surname}
                                </Typography>

                            </div>
                        </div>

                    </div>
                </div>
                <div className="container">
                    <div className="columns">
                        <div className="column is-8">
                            <Typography component="h2" variant="h3" align="left" color="inherit">
                                Mis subidas ({total})
                            </Typography>
                        </div>
                        <div className="column is-4">
                                <div className="card seguidores">
                                        
                                        <footer className="card-footer">
                                            <p className="card-footer-item">
                                                Seguidores <br /> <b>{followed}</b>
                                            </p>
                                            <p className="card-footer-item">
                                                Siguiendo <br /> <b>{following}</b>
                                            </p>
                                            <p className="card-footer-item">
                                                Media <br /> <b>{total}</b>
                                            </p>
                                        </footer>
                                </div>
                            </div>
                        </div>

                        {/* Content */}

                        {content}
                        
                        
                            
                    <nav className="pagination is-medium" role="navigation" aria-label="pagination">
                        {this.createPaginator()}
                        <ul className="pagination-list">
                            
                            {this.createPages()}
                        </ul>
                    </nav>
                    <hr />
                </div>
            </div>
        );
    };
}
Perfil.propTypes = {
    classes: PropTypes.object.isRequired,
  };


export default 
    withStyles(styles)(
    (withRouter(Perfil))
);