import React,{Component} from 'react';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import CssBaseline from '@material-ui/core/CssBaseline';
import AudioPlayer from "react-h5-audio-player";
import { BrowserRouter as Router, Route, NavLink, Redirect, withRouter,Switch } from 'react-router-dom';
import Resumen from './resumen';
import Historial from './historial';
import Listas from './listas';
import MeGusta from './megusta';
import Siguiendo from './siguiendo';


const styles = theme => ({
    main: {
      width: 'auto',
      marginTop:theme.spacing.unit*2,
      display: 'block', // Fix IE 11 issue.
      marginLeft: theme.spacing.unit * 3,
      marginRight: theme.spacing.unit * 3,
      [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
        marginLeft: 'auto',
        marginRight: 'auto',
      },
    },
    content: {
        marginTop:theme.spacing.unit * 2,
        marginLeft: theme.spacing.unit * 3,
    },
    root: {
        flexGrow: 1,
      },
      paper: {
        height: 140,
        width: 100,
      },
      control: {
        padding: theme.spacing.unit * 2,
      },
  });


function getTypeContent(type) {
    if(type){
        type = type.toLowerCase()
    }
    switch (type) {
        case 'resumen':
            return <Resumen></Resumen>;
        case 'megusta':
            return <MeGusta></MeGusta>;
        case 'listas':
            return <Listas></Listas>;
        case 'siguiendo':
            return <Siguiendo></Siguiendo>;
        case 'historial':
            return <Historial></Historial>;
        default:
            return <Resumen></Resumen>;
    }
}

class Coleccion extends Component {
    constructor(props){
        super(props)
    }

    render() {
        const { classes,match } = this.props;
        return (

            <main className={classes.main}>
            <CssBaseline />
                <Grid 
                    container 
                    justify="center"
                    >
                    <Grid item xs={10}>
                        <Grid
                        container
                        justify="left" 
                        alignItems="left" 
                        >
                            <Typography className={classes.content} paragraph color="textSecondary" component="h2" variant="h5" align="center">
                                <NavLink className="colection-links" to="/coleccion/resumen">Resumen</NavLink>
                            </Typography>
                            <Typography className={classes.content} paragraph color="textSecondary" component="h2" variant="h5" align="center">
                                <NavLink className="colection-links" to="/coleccion/megusta">Me gusta</NavLink>
                            </Typography>
                            <Typography className={classes.content} paragraph color="textSecondary" component="h2" variant="h5" align="center">
                                <NavLink className="colection-links" to="/coleccion/listas">Listas</NavLink>
                            </Typography>
                            <Typography className={classes.content} paragraph color="textSecondary" component="h2" variant="h5" align="center">
                                <NavLink className="colection-links" to="/coleccion/siguiendo">Siguiendo</NavLink>
                            </Typography>
                            <Typography className={classes.content} paragraph color="textSecondary" component="h2" variant="h5" align="center">
                                <NavLink className="colection-links" to="/coleccion/historial">Historial</NavLink>
                            </Typography>
                        </Grid>                        
                    </Grid>
                    
                    
                </Grid>
                <div className="container">
                    {getTypeContent(match.match.params.tipo)}
                </div>
            </main>

        );



    }



}

export default withStyles(styles)(Coleccion);