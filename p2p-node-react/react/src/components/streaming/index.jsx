import React, { Component } from 'react';
import PropTypes, { func } from 'prop-types';
import MediaHandler from '../../services/MediaHandler';
import withStyles from '@material-ui/core/styles/withStyles';
import openSocket from 'socket.io-client';


const socket = openSocket('http://localhost:4000/stream')

const styles = theme => ({
  main: {
    width: 'auto',
    display: 'block',
    marginTop:theme.spacing.unit * 2,
  },
});

class Streaming extends Component {
  constructor(props){
    super(props)
    
    this.state = {
      paused: false,
      signalData: {},
    }
    this.mediaHandler = new MediaHandler;
  }
  componentWillMount(){
    this.mediaHandler.getPermissions().then((stream) => {
      var canvas = this.canvas;
      var context = canvas.getContext("2d")
      this.setState({hasMedia: true});
      try{
        this.myVideo.srcObject = stream;
      } catch(e){
        this.myVideo.src = URL.createObjectURL(stream);
      }
      this.myVideo.play();
      var video = document.getElementById('video')

      video.addEventListener('play', () => {
        console.log('asd')
      })
      video.addEventListener('pause', () => {
        console.log('pause')
      })
      setInterval(() => {
        context.drawImage(video,0,0,100,100);
        socket.emit("streaming-client",canvas.toDataURL('image/webp'))
      },70)      

    });

  }


  
  render() {
    const { classes } = this.props;
    const { paused } = this.state;
    return (
      <main className={classes.main+" centered"}>
        <div className="container">
          <div className="columns">
            <div className="column is-12">
              <div style={{textAlign:'center'}} className="video-container">
                <video muted controls style={{width:'80%'}} id="video" className="my-video" ref={(ref) => {this.myVideo = ref;}}></video>
                <div style={{position:'absolute',top:'4%',width:'100%'}}>
                  NICK
                </div>
                <canvas id="canvas" className="my-canvas" ref={(ref) => {this.canvas = ref;}}></canvas>
              </div>
            </div>
          </div>
          
        </div>
      </main>
    );
  }
}

Streaming.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Streaming)