import React, { Component } from 'react';
import openSocket from 'socket.io-client';
import Moment from "react-moment";
import Badge from '@material-ui/core/Badge';
import { Link } from 'react-router-dom';

const socket = openSocket('http://localhost:4000')

class Chat extends Component {
  constructor(props){
    super(props);
    this.state = {
      toggle: false,
      messages: [],
      count:0,
      identity: JSON.parse(localStorage.getItem('identity')),
    }

  }
  componentDidMount(){
    if(localStorage.getItem('identity')){
      socket.emit("user",JSON.parse(localStorage.getItem('identity'))._id);
    }
    socket.on("receive-message",(msg) => {
      if(this.state.count===0){
        document.title = "StreamMedia";
      }else{
        document.title = "("+this.state.count+") StreamMedia";
      }
      this.state.messages.push(msg);
      this.state.messages.reverse() // Revertir array
      if(this.state.toggle){
        this.setState({
          count: 0,
        })
      }else{
        this.setState({
          count: this.state.count+1,
        })
      }
    })
    
    return {
      socket: socket
    }
  }
  
  toggle(){
    this.setState({
      toggle: !this.state.toggle,
      count: 0,
    })
  }
  handlePress(event){
    event.preventDefault();
    var msg = document.getElementById("sendMessage").value;
    if(msg.length > 0){
      socket.emit("new-message",msg,JSON.parse(localStorage.getItem('identity')).nick,JSON.parse(localStorage.getItem('identity'))._id);
    }
    document.getElementById("chat-form").reset(); 
  }


  render() {
    const { toggle,messages,count,identity } = this.state;


    console.log(count)
    return (
      <div id="chat">
        <div  onClick={this.toggle.bind(this)}>
        &nbsp;Chat
        <Badge badgeContent={count} color="secondary" />
          
        </div>

        { toggle && (
          <div>
            <div class="card">
            {
              messages.length == 0 && (
                <div class="content">
                  Envia tu primer mensaje
                </div>
              )
            }
            {messages.map(message =>
               <div>
               { message._id == identity._id ?
                  <div class="content">
                    <Link style={{color:'black'}} to={"/perfil/"+message._id}><b>{message.from}</b></Link><br/>{message.msg}
                    <br />
                    <small>
                      <Moment unix format={"HH:mm:ss -- DD-MM-YYYY"}>
                        {message.created_at}
                      </Moment>
                    </small>
                  </div>
                  :
                  <div style={{textAlign:'right'}} class="content">
                    <Link style={{color:'black'}} to={"/perfil/"+message._id}><b>{message.from}</b></Link><br/>{message.msg}
                    <br />
                    <small>
                      <Moment unix format={"HH:mm:ss -- DD-MM-YYYY"}>
                        {message.created_at}
                      </Moment>
                    </small>
                  </div>
                  
                  }
                  </div>
                  )}
            </div>
            
          </div>
          
          
          )}
          { toggle && (
          <form  autocomplete="off" id="chat-form" onSubmit={this.handlePress.bind(this)}>
            <div class="field">
              <div class="control">
                <input id="sendMessage" class="input is-primary" type="text" placeholder="Enviar mensaje..." />
              </div>
            </div>
          </form>
          )}
      </div>
    );
  }
}

export default Chat;
