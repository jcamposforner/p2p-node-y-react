import React,{Component} from 'react';
import PropTypes from 'prop-types';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import LockIcon from '@material-ui/icons/LockOutlined';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {userLogged} from "../../actions";
import { withRouter } from 'react-router-dom';
import axios from 'axios';
import { urlApi } from '../../env'

const styles = theme => ({
    main: {
      width: 'auto',
      display: 'block', // Fix IE 11 issue.
      marginLeft: theme.spacing.unit * 3,
      marginRight: theme.spacing.unit * 3,
      [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
        width: 400,
        marginLeft: 'auto',
        marginRight: 'auto',
      },
    },
    paper: {
      marginTop: theme.spacing.unit * 8,
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
    },
    avatar: {
      margin: theme.spacing.unit,
      backgroundColor: theme.palette.secondary.main,
    },
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing.unit,
    },
    submit: {
      marginTop: theme.spacing.unit * 3,
    },
  });


class Registro extends Component {

    constructor(props){
        super(props);
        this.state = {
            name:'',
            surname:'',
            nick:'',
            email:'',
            password:'',
        }
        this.handleSubmit = this.handleSubmit.bind(this);
    }


    handleSubmit(e) {
        e.preventDefault();
        const formData = {
            name: document.getElementById("name").value,
            surname: document.getElementById("surname").value,
            nick: document.getElementById("nick").value,
            email: document.getElementById("email").value,
            password: document.getElementById("password").value,
        }

        const params = {
            email: document.getElementById("email").value,
            password: document.getElementById("password").value,
        }

        axios.post(`${urlApi}register`, formData).then(
            (res) => {
                this.logIn(params)
            },
            (err) => console.log(err)
        )
        
    }

    logIn(params){
        axios.post(`${urlApi}login`, params).then(
            (res) => {
                localStorage.setItem('identity',JSON.stringify(res.data.user));
                this.getToken(params);
            },
            (err) => console.log(err)
        )
    }

    getToken(params){
        params['gettoken'] = true;
        axios.post(`${urlApi}login`, params).then(
            (res) => {
                localStorage.setItem('token',JSON.stringify(res.data.token));
                this.props.userLogged();
                window.location.replace("/");
            },
            (err) => console.log(err)
        )
    }

    render (){
        const { classes } = this.props;

        return (
            <main className={classes.main+" centered"}>
            <CssBaseline />
            <Paper className={classes.paper}>
                <Avatar className={classes.avatar}>
                <LockIcon />
                </Avatar>
                <Typography component="h1" variant="h5">
                Registro
                </Typography>
                <form onSubmit={this.handleSubmit} className={classes.form}>
                    <FormControl margin="normal" required fullWidth>
                        <InputLabel htmlFor="name">Nombre</InputLabel>
                        <Input id="name" name="name" autoComplete="name" autoFocus />
                    </FormControl>
                    <FormControl margin="normal" required fullWidth>
                        <InputLabel htmlFor="surname">Apellidos</InputLabel>
                        <Input id="surname" name="surname" autoComplete="surname" />
                    </FormControl>
                    <FormControl margin="normal" required fullWidth>
                        <InputLabel htmlFor="nick">Nick</InputLabel>
                        <Input id="nick" name="nick" autoComplete="nick" />
                    </FormControl>
                    <FormControl margin="normal" required fullWidth>
                        <InputLabel htmlFor="email">Correo Electronico</InputLabel>
                        <Input id="email" name="email" autoComplete="email" />
                    </FormControl>
                    <FormControl margin="normal" required fullWidth>
                        <InputLabel htmlFor="password">Contraseña</InputLabel>
                        <Input name="password" type="password" id="password" autoComplete="current-password" />
                    </FormControl>
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                    >
                    Registrarse
                </Button>
                </form>
            </Paper>
            </main>
        );
    };
}
Registro.propTypes = {
    classes: PropTypes.object.isRequired,
  };
function mapStateToProps(state) {
    return {
        logged: state.userLogged,
    }
}
function matchDispatchToProps(dispatch){
    return bindActionCreators({userLogged:userLogged}, dispatch);
}


export default 
    withStyles(styles)(
    connect(mapStateToProps,matchDispatchToProps)(withRouter(Registro))
);