'use strict'

var express = require('express');
var MediaController = require('../controllers/media');

var md_auth = require('../middlewares/authenticated');
var api = express.Router();

// Subida imagen

var multipart = require('connect-multiparty');
var md_upload = multipart({uploadDir: 'uploads/media'});
var md_uploadThumbmail = multipart({uploadDir: 'uploads/thumbmails'});



api.get('/media/:slug',md_auth.ensureAuth, MediaController.getMedia);
api.get('/medias/:page?',md_auth.ensureAuth, MediaController.getMedias);
api.get('/my-medias/:page?',md_auth.ensureAuth, MediaController.getMyMedias);
api.get('/medias/:style/:page?',md_auth.ensureAuth, MediaController.getMediasByStyle);
api.get('/get-media/:mediaFile', MediaController.getMediaFile);
api.get('/get-thumbmail/:mediaFile', MediaController.getThumbmailFile);
api.get('/profile-medias/:id/:page?',md_auth.ensureAuth, MediaController.getMediasByUser)


api.post('/upload-media',md_auth.ensureAuth, MediaController.saveMedia);
api.post('/upload-video/:id',[md_auth.ensureAuth, md_upload], MediaController.uploadMedia);
api.post('/upload-thumbmail/:id',[md_auth.ensureAuth, md_uploadThumbmail], MediaController.uploadThumbmail);


module.exports = api;