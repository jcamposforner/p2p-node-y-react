'use strict'

var mongoosePaginate = require('mongoose-pagination');
var moment = require('moment');


// Primera letra mayuscula para saber que es un modelo
var Votes = require('../models/votes');
var User = require('../models/votes');
var jwt = require('../services/jwt');


function saveVotes(req,res){
    //Campos de POST
    var params = req.body;
    // Creamos nuevo usuario
    var votes = new Votes();

    if(params.media){

        votes.user = req.user.sub;
        votes.media = params.media;
        votes.created_at = moment().unix();
        
        Votes.countDocuments({$and: [
            {user: req.user.sub},
            {media:params.media}
        ]}).exec((err,vote)=>{
            if(err) return res.status(500).send({message:'Ha ocurrido un error al guardar el voto'});
            if(!vote > 0){
        
            votes.save((err,votesStored) => {
                if(err) return res.status(500).send({message:'Ha ocurrido un error al guardar el voto'});

                if(votesStored){
                    res.status(200).send({voto: votesStored});
                }else{
                    res.status(404).send({message:'No se ha registrado el voto'});
                }
            });
        }else{
            res.status(200).send({message:'Ya te gusta este video'});
        }
    })

    }else{
        res.status(200).send({
           message: 'Rellena todos los datos',
        });
    }

}

function getMyLikes(req,res){

    var page = 1;
    if(req.params.page){
        page = req.params.page;
    }

    var itemsPerPage = 12;

    Votes.find({user:req.user.sub}).select('-_id -__v').sort('-created_at').populate('media',' -__v').deepPopulate('media.user', '-user._id -__v -password -role').paginate(page, itemsPerPage, (err,videos, total) => {
        if(err) return res.status(500).send({message: 'Error en la petición'});

        if(!videos) return res.status(404).send({message: 'No hay videos disponibles'});
        
        videos.forEach(video => {
            video.media.user.password = undefined;
            video.media.user.role = undefined
        });

        return res.status(200).send({
            videos,
            currentPage:page,
            total,
            pages: Math.ceil(total/itemsPerPage),
        });
    });
}

module.exports = {
    saveVotes,
    getMyLikes
};