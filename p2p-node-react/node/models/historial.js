'use strict'

var mongoose = require('mongoose');
var deepPopulate = require('mongoose-deep-populate')(mongoose);
var Schema = mongoose.Schema;

var HistorialSchema = Schema({
    user:{ type: Schema.ObjectId, ref: 'User' },
    media: { type: Schema.ObjectId, ref: 'Media' },
    created_at: String,
});


HistorialSchema.plugin(deepPopulate)
module.exports = mongoose.model('Historial', HistorialSchema);